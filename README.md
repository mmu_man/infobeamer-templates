# Infobeamer Templates

{33,34,*}C3 [InfoBeamer](https://info-beamer.com/blog/running-the-34c3-info-beamer-screens) templates for LQDN and friends.

![](small_34C3_ad_TeaHouse.jpg)

Includes, but not limited to, ads for the Quadra'TeaHouse, the CHATONS collective, the Degooglize campaign from Framasoft…

Most are either Inkscape SVG files with some linked bitmaps, or Makefiles that generate PNG/JPG images or even small videos.


## Dependencies

* Inkscape for the SVG templates
* GNU Make
* Wget
* ImageMagick
* qrencode
* ffmpeg for videos
* TTF fonts:
  * Cantarell
  * Propaganda (LQDN)
  * Alegreya (hackerspaces.org)
  * Monotype Corsiva (TeaHouse)
  * 8-bit Limit O BRK (RetroComputing Devroom)
  * Amiga Topaz Unicode Rus (RetroComputing Devroom)
  * Montserrat (35C3) (apt:texlive-fonts-extra & might require [some fc-cache trickery](https://tex.stackexchange.com/questions/361595/xelatex-fc-list-fc-cache-problem))
  * Roboto (36C3 LQDN) (apt:fonts-roboto)
  * IBM 3270 (rC3 RetroComputing Devroom) (apt:fonts-pc-extra)
  * [Compagnon fonts](https://velvetyne.fr/fonts/compagnon/) (rC3 LQDN) (apt:fonts-compagnon but I actually had to remove it and install Compagnon-Roman.otf only manually else Inkscape wouldn't find it.)


## rC3 2021

Infobeamer proposals :
* [FOSDEM RetroComputing Devroom](rC3-2021/fosdem_retro).
* [La Quadrature](rC3-2021/lqdn).


## 36C3

Infobeamer proposals :
* [InfoBeamer](36C3/infobeamer) "Your ad here.".
* [Hackerspaces.org](36C3/hackerspaces).
* [FOSDEM RetroComputing Devroom](36C3/fosdem_retro).
* [Giggity](36C3/giggity).
* [La Quadrature's TeaHouse](36C3/teahouse).
* [La Quadrature](36C3/lqdn) (donation campaign).

## 35C3

Infobeamer proposals :
* [InfoBeamer](35C3/infobeamer) "Your ad here.".
* [La Quadrature's TeaHouse](35C3/teahouse).
* [La Quadrature's GAFAM posters campaign](35C3/gafam).
* [Hackerspaces.org](35C3/hackerspaces).
* [Giggity](35C3/giggity) (event schedule app for Android).
* [OpenFoodFacts](35C3/off) (open food products database).
* [FOSDEM RetroComputing Devroom](35C3/fosdem_retro).
* [FOSDEM Decentralized & Privacy Devroom](35C3/fosdem_decentralized).
* [FOSDEM Hardware Enablement Devroom](35C3/fosdem_hardware).
* [La Quadrature](35C3/lqdn) (donation campaign).
* [Framasoft](35C3/framasoft) (donation campaign).

## 34C3

Infobeamer proposals :
* [Contributopia](34C3/contributopia) campaign by Framasoft.
* [La Quadrature 2017 support campaign](34C3/lqdn) using moto generator samples.
* [RMLL](34C3/rmll) (Libre Software Meeting / Rencontres Mondiales du Logiciel Libre).
* [La Quadrature's TeaHouse](34C3/teahouse).
* [Framasky's WemaWema](34C3/wema) meme generator.

## 33C3

[Infobeamer templates](33C3).
TODO: fix missing bitmaps.

